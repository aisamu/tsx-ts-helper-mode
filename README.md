# tsx-ts-helper-mode

A helper mode for the new `ts-tsx-helper-mode` in Emacs 29. Currently it provides:

- auto-closing of tags
- renaming of tags (`C-c t r`)
- deleting inner contents of tags (`C-c t d i`)
- deleting complete tags (`C-c t d t`)
- deleting attributes (`C-c t d a`)

## Installation using straight

```
(use-package tsx-ts-helper-mode
  :straight (tsx-ts-helper-mode
             :type git
             :host codeberg
             :repo "ckruse/tsx-ts-helper-mode")
  :commands (tsx-ts-helper-mode)
  :hook (tsx-ts-mode . tsx-ts-helper-mode))
```

You can change the mode map prefix by setting `tsx-ts-helper-mode-keymap-prefix`:

```
(use-package tsx-ts-helper-mode
  :straight (tsx-ts-helper-mode
             :type git
             :host codeberg
             :repo "ckruse/tsx-ts-helper-mode")
  :commands (tsx-ts-helper-mode)
  :custom (tsx-ts-helper-mode-keymap-prefix (kbd "C-c C-t"))
  :hook (tsx-ts-mode . tsx-ts-helper-mode))
```

You can also disable auto-closing of tags by setting `tsx-ts-helper-mode-auto-close-tags` to `nil`:

```
(use-package tsx-ts-helper-mode
  :straight (tsx-ts-helper-mode
             :type git
             :host codeberg
             :repo "ckruse/tsx-ts-helper-mode")
  :commands (tsx-ts-helper-mode)
  :custom (tsx-ts-helper-mode-auto-close-tags nil)
  :hook (tsx-ts-mode . tsx-ts-helper-mode))
```

## installation using `package.el`

Installation via package.el is not (yet?) supported; I don’t know shit about how to get it to melpa or elpa.
